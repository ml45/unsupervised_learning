import pandas as pd

from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import OneHotEncoder
from sklearn.model_selection import train_test_split

def preprocess_dodgers(random_state=1337):
    dataset1_fname = "dodgers.csv"
    dataset1 = pd.read_csv(dataset1_fname)

    dataset1 = dataset1[["pitch_type", "release_speed", "release_pos_x", "release_pos_z", "description", "p_throws",
                         "balls", "strikes",
                         "pfx_x", "pfx_z","vx0","vy0","vz0","ax","ay","az","sz_top","sz_bot",
                         "effective_speed", "release_spin_rate", "release_pos_y", "at_bat_number", "pitch_number",
                         "plate_x", "plate_z"]]

    dataset1 = dataset1.dropna()

    # https://towardsdatascience.com/categorical-encoding-using-label-encoding-and-one-hot-encoder-911ef77fb5bd
    dataset1_to1hot = ["pitch_type", "p_throws"]
    enc = OneHotEncoder()
    enc_df = pd.DataFrame(enc.fit_transform(dataset1[dataset1_to1hot]).toarray())
    dataset1 = dataset1.join(enc_df)
    dataset1 = dataset1.drop(["pitch_type", "p_throws"], axis=1)
    dataset1 = dataset1.dropna()

    dataset1_X = dataset1.drop(["description"], axis=1)
    dataset1_y = dataset1["description"]
    
    X_train, X_test, y_train, y_test = train_test_split(dataset1_X, dataset1_y, test_size=0.2, random_state=random_state)
    scaler = StandardScaler().fit(X_train)
    X_train = scaler.transform(X_train)
    X_train = pd.DataFrame(X_train, columns = dataset1_X.columns)  # new: keep old column names

    scaler.fit(X_test)
    X_test = scaler.transform(X_test)
    X_test = pd.DataFrame(X_test, columns = dataset1_X.columns)

    return X_train, X_test, y_train, y_test

def preprocess_wine(random_state=1337):
    dataset = pd.read_csv("winequality-red.csv", delimiter=";", header=0)
    dataset = dataset.dropna()
    dataset_X = dataset.drop(["quality"], axis=1)
    dataset_y = dataset["quality"]

    X_train, X_test, y_train, y_test = train_test_split(dataset_X, dataset_y, test_size = 0.2, random_state=random_state)

    scaler = StandardScaler().fit(X_train)
    X_train = scaler.transform(X_train)
    X_train = pd.DataFrame(X_train, columns = dataset_X.columns)  # new: keep old column names

    scaler.fit(X_test)
    X_test = scaler.transform(X_test)
    return X_train, X_test, y_train, y_test
    
def preprocess_wine_combo(random_state=1337):
    dataset = pd.read_csv("winequality-combined.csv", delimiter=",", header=0)
    dataset = dataset.dropna()
    dataset_X = dataset.drop(["quality"], axis=1)
    dataset_y = dataset["quality"]

    col_headers = list(dataset_X.columns)
    col_headers.remove("color")


    X_train, X_test, y_train, y_test = train_test_split(dataset_X, dataset_y, test_size = 0.2, random_state=random_state)
    X_train_color = X_train["color"]
    X_train = X_train.drop(["color"], axis=1)

    X_test_color = X_test["color"]
    X_test = X_test.drop(["color"], axis=1)


    scaler = StandardScaler().fit(X_train)
    X_train = scaler.transform(X_train)
    X_train = pd.DataFrame(X_train, columns = col_headers)  # new: keep old column names

    scaler.fit(X_test)
    X_test = scaler.transform(X_test)
    X_test = pd.DataFrame(X_test, columns = col_headers)  # new: keep old column names
    return X_train, X_test, y_train, y_test, X_train_color, X_test_color