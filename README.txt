# Assignment 3 - Unsupervised Learning and Dimensionality Reduction

## About
Timothy Isonio

tisonio3@gatech.edu

CS 7641 - Machine Learning - Spring 2021
Code and extras located at:
https://gitlab.com/ml45/unsupervised_learning

This is a Markdown document with a .txt extension.

## Environment
In the code repo is the environment.yml needed to replicate the Conda environment I used
for this project. If that does not work, in essence creating an environment with these:

    python>=3.8 numpy scipy scikit-learn>=0.24 matplotlib pandas jupyter yellowbrick seaborn

should work too. I did this project on a Windows 10 PC running an AMD 8-core/16-thread processor,
so there might be some Windows specific stuff in there. Sometimes I hard-coded 14 jobs
so that I had a core to actually use myself while it ran.

## Running the code
Everything is split into Jupyter notebooks. To run, just type 

    jupyter notebook

in this directory. They're all named what they do, part1, part2, etc.

Most of them one of the first few cells has you comment/uncomment which dataset to run on.

## References
* Essentially all of the Scikit-learn docs.
* Wine quality dataset from UCI
* Baseball Statcast dataset
* The articles also cited in the report